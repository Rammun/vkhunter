﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace VkHunter.Common.Helpers
{
    public class CommonHelper
    {
        /// <summary>
        /// Получает значение параметра настроек приложения по ключу.
        /// </summary>
        /// <param name="key">Ключ параметра настроек.</param>
        /// <returns>Значение параметра настроек приложения.</returns>
        public static string GetConfigByKey(string key)
        {
            var appSettings = ConfigurationManager.AppSettings;

            return appSettings[key];
        }

        /// <summary>
        /// Возвращает значение параметра настроек приложения по ключу в app.config,
        /// если отсутствует - значение по умолчанию.
        /// </summary>
        /// <param name="key">Ключ параметра настроек в app.config.</param>
        /// <param name="defaultVale">Значение по умолчанию</param>
        /// <returns>Значение параметра настроек приложения.</returns>
        public static string GetConfigOrDefault(string key, string defaultVale)
        {
            return ConfigurationManager.AppSettings.AllKeys.Contains(key)
                ? ConfigurationManager.AppSettings[key]
                : defaultVale;
        }

        public static async Task SendEmail(string destination, string body)
        {
            const string from = "admin@market.com";
            const string subject = "Market Service";

            // создаем письмо: message.Destination - адрес получателя
            var mail = new MailMessage(from, destination, subject, body);

            var client = new SmtpClient();
            await client.SendMailAsync(mail);
        }
    }
}
