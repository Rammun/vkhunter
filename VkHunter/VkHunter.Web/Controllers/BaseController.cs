﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using VkHunter.Domain;
using VkHunter.Domain.Identity;

namespace VkHunter.Web.Controllers
{
    public class BaseController : ApiController
    {
        private readonly VkDbContext _dbContext;

        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        protected ApplicationUserManager UserManager =>
            _userManager ?? (_userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>());

        protected ApplicationRoleManager RoleManager =>
            _roleManager ?? (_roleManager = Request.GetOwinContext().Get<ApplicationRoleManager>());

        #region DISPOSE

        private bool _disposed = false;

        public new void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this._disposed = true;
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
