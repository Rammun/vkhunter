﻿namespace VkHunter.Web.Constants
{
    public class ParamName
    {
        /// <summary>
        /// Имя ключа параметра, указывающий основной адрес
        /// </summary>
        public const string BaseUrlKey = "baseUrl";

        /// <summary>
        /// Имя ключа параметра, указывающий время обновления данных по расписанию
        /// </summary>
        public const string TimeUpdateKey = "timeUpdate";

        /// <summary>
        /// Имя ключа параметра, указывающий email администратора
        /// </summary>
        public const string AdminUserName = "adminUser";
    }
}
