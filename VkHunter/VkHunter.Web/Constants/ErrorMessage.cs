﻿namespace VkHunter.Web.Constants
{
    public class ErrorMessage
    {
        public const string IncorrectParameters = "Incorrect parameters";
        public const string Success = "Success";
        public const string Unsuccess = "Unsuccess";
        public const string During = "During task";
        public const string Completed = "Completed";
    }
}
