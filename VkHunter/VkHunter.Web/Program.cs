﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using VkHunter.Common.Helpers;
using VkHunter.Domain;
using VkHunter.Domain.Entities;
using VkHunter.Web.Constants;

namespace VkHunter.Web
{
    class Program
    {
        static void Main(string[] args)
        {
            var baseAddress = CommonHelper.GetConfigOrDefault(ParamName.BaseUrlKey, DefaultSetting.BaseUrl);

            // Start OWIN host 
            using (WebApp.Start<Startup>(url: baseAddress))
            {
                Console.WriteLine("Begin...");

                var queryResult = TestQuery();
                var dbResult = TestDB();

                if (true)// (queryResult && dbResult)
                {
                    Console.WriteLine("\nПриложение запущено...");

                    #region DATA AUTO UPDATE

                    var updateTime = CommonHelper.GetConfigOrDefault(ParamName.TimeUpdateKey, DefaultSetting.TimeUpdate);

                    var callback = new TimerCallback(Worker);
                    var timer = new Timer(callback, updateTime, new TimeSpan(0, 1, 0), new TimeSpan(0, 1, 0));

                    #endregion
                }

                Console.ReadLine();
            }


        }

        private static void Worker(object updateTime)
        {
            var dateTimeNow = DateTime.Now;

            if (dateTimeNow.ToShortTimeString() != updateTime.ToString())
                return;

            var message = $"{dateTimeNow}: обновление данных по отслеживаемым моделям...";

            Console.WriteLine(message);
            //_logger.Trace(message);

            //var marketService = new ApiMarketService(new MarketContext());
            //try
            //{
            //    marketService.UpdateDataBySchedule().Wait();
            //}
            //catch (Exception ex)
            //{
            //    var errorMessage = $"{nameof(Program)} | {nameof(Worker)} | {ex}";
            //    Console.WriteLine(errorMessage);
            //    _logger.Trace(errorMessage);
            //    CommonHelper.SendEmail(CommonHelper.GetConfigByKey(ParamName.AdminUserName), errorMessage).Wait();
            //    return;
            //}

            Console.WriteLine("OK");
        }

        private static bool TestQuery()
        {
            using (var client = new HttpClient())
            {
                Console.WriteLine("Тест запроса:");

                var baseAddress = CommonHelper.GetConfigOrDefault(ParamName.BaseUrlKey, DefaultSetting.BaseUrl);
                var response = client.GetAsync(baseAddress + "api/test/getValues").Result;

                Console.WriteLine(response);

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Тест не пройден!");
                    return false;
                }

                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Console.WriteLine("OK");

                return true;
            }
        }

        private static bool TestDB()
        {
            using (var dbContext = new VkDbContext())
            {
                Console.WriteLine("\nТест БД (запись/чтение/удаление):");
                
                try
                {
                    var model = dbContext.TestModels.Add(new TestModel
                    {
                        Name = "Test DB"
                    });

                    var models = dbContext.TestModels.ToList();
                    foreach (var m in models)
                    {
                        Console.WriteLine($"{m.Id}   {m.Name}");
                        dbContext.TestModels.Remove(m);
                    }

                    Console.WriteLine("OK");
                    return true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Тест не пройден!");
                    return false;
                }
            }
        }
    }
}
