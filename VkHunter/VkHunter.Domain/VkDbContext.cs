﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using VkHunter.Domain.Entities;

namespace VkHunter.Domain
{
    public class VkDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<TestModel> TestModels { get; set; }

        static VkDbContext()
        {
            Database.SetInitializer(new DbInitializer());
            using (var db = new VkDbContext())
                db.Database.Initialize(false);
        }

        public VkDbContext()
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public VkDbContext(string connection)
             : base(connection)
        { }

        public static VkDbContext Create()
        {
            var dbContext = new VkDbContext();
            return dbContext;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Vendor>()
            //    .HasMany(x => x.Categories)
            //    .WithMany(x => x.Vendors);

            base.OnModelCreating(modelBuilder);
        }

    }
}
