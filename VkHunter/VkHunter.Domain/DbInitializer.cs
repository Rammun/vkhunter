﻿using System.Configuration;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using VkHunter.Domain.Entities;

namespace VkHunter.Domain
{
    internal class DbInitializer : DropCreateDatabaseIfModelChanges<VkDbContext> // DropCreateDatabaseIfModelChanges  CreateDatabaseIfNotExists
    {
        public override void InitializeDatabase(VkDbContext context)
        {
            base.InitializeDatabase(context);
            
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var userName = ConfigurationManager.AppSettings["adminUser"];
            var password = ConfigurationManager.AppSettings["adminPassword"];

            var user = new ApplicationUser
            {
                UserName = userName,
                Email = userName,
                EmailConfirmed = true
            };
            userManager.Create(user, password);

            context.SaveChanges();
        }
    }
}
